<?php

namespace App\VO;

class Post
{
	private $allowFileds = [
		'title',
		'user',
		'text',
		'date',
	];

	private $data;

	public function __construct(array $post)
	{
		foreach ($this->allowFileds as $field) {
			if (!empty($post[$field])) {
				$this->$field = $post[$field];
			}
		}
	}

	public function __get($field)
	{
		return $data[$field] ?? '';
	}
}