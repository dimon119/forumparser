<?php

namespace App\Service;

use \GuzzleHttp\Client;
use \GuzzleHttp\Cookie\CookieJar;

class Crawler
{
	public function createAuthCookie(): CookieJar
	{
		$jar = new CookieJar;
		$client = new Client([
			'cookies' => $jar,
		]);

		$loginPage = Config::get('FORUM_LOGIN_URL');
		$login = Config::get('FORUM_LOGIN');
		$password = Config::get('FORUM_PASSWORD');
		$md5Password = md5($password);

		$response = $client->request('POST', $loginPage, [
			'form_params' => [
				"do" => "login",
				"vb_login_md5password" => $md5Password,
				"vb_login_md5password_utf" => $md5Password,
				"vb_login_username" => $login,
			],
		]);
		return $jar;
	}

	public function openPage(string $link, CookieJar $jar): string
	{
		$client = new Client([
			'cookies' => $jar,
		]);
		$threadResponse = $client->request('GET', $link);
		$page = $threadResponse->getBody()->getContents();
		return $page;
	}
}