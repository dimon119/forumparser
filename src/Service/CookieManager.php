<?php

namespace App\Service;

use \InvalidArgumentException;
use \GuzzleHttp\Cookie\CookieJar;

class CookieManager
{
	private static $cookieFilePath = BASE_DIR . '/storage/tmp/cookie.txt';

	public static function saveCookie(CookieJar $cookie): bool
	{
		$cookie = serialize($cookie);
		return file_put_contents(self::$cookieFilePath, $cookie);
	}

	public static function getCookie(): CookieJar
	{
		if (!file_exists(self::$cookieFilePath)) {
			throw new InvalidArgumentException('Cookie file not found!');
		}
		$cookie = file_get_contents(self::$cookieFilePath);
		if (empty($cookie)) {
			throw new InvalidArgumentException('Cookie file is empty!');
		}
		$jar = unserialize($cookie);
		return $jar;
	}
}