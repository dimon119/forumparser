<?php

namespace App\Service;

use App\VO\Post;

class FileManager
{
	private $path = BASE_DIR . '/storage/posts/';

	private function generateFileName(Post $data): string
	{
		$title = substr($data->title, 0, 200);
		$title = str_replace('/', ' ', $title);
		return $this->path . $title. ' ' . $data->date . '.json';
	}

	public function savePostInFile(Post $data): bool
	{
		$fileName = $this->generateFileName($data);
		$json = json_encode($data);
		return file_put_contents($fileName, $json);
	}
}