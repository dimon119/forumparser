<?php

namespace App\Service;

use App\VO\Post as PostVO;
use voku\helper\HtmlDomParser;

class DomParser
{
	public function parsePostLinksFromThread(string $html): array
	{
		$links = [];
		$dom = HtmlDomParser::str_get_html($html);
		$posts = $dom->findMulti('a.title');
		foreach ($posts as $post) {
			$links[] = $post->baseURI . $post->href;
		}
		return $links;
	}

	public function parsePost(string $html): PostVO
	{
		$postData = [];
		$dom = HtmlDomParser::str_get_html($html);
		$data = $dom->findMulti('.posts li');
		$firstPost = reset($data);

		$postData['user'] = reset($firstPost->find('.postdetails .username_container a.username strong')->innertext);
		$postData['title'] = reset($firstPost->find('.postdetails .postrow h2.title')->plaintext);
		$postData['text'] = reset($firstPost->find('.postdetails .content blockquote')->plaintext);
		$date = reset($firstPost->find('.posthead span.postdate.old span')->plaintext);
		$postData['date'] = str_replace("&nbsp;", ' ', $date);
		return new PostVO($postData);
	}
}