<?php

namespace App\Service;

use InvalidArgumentException;

class Config
{
	public static function get(string $key): string
	{
		if (empty($_ENV[$key])) {
			throw new InvalidArgumentException("Config '$key' not exists");
		}
		return $_ENV[$key];
	}
}