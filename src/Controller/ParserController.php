<?php

namespace App\Controller;

use App\Service\Config;
use App\Service\CookieManager;
use App\Service\Crawler;
use App\Service\DomParser;
use App\Service\FileManager;

class ParserController
{
	public $crawler;
	public $domParser;
	public $fileManager;

	public function __construct()
	{
		$this->crawler = new Crawler();
		$this->domParser = new DomParser();
		$this->fileManager = new FileManager();
	}

	public function login()
	{
		$cookie = $this->crawler->createAuthCookie();
		$save = CookieManager::saveCookie($cookie);
		if ($save) {
			echo 'Cookie успешно сохранены';
		} else {
			echo 'Ошибка при сохранении Cookie!';
		}
	}

	public function parse(int $threadId)
	{
		$jar = CookieManager::getCookie();
		$threadPage = Config::get('FORUM_THREAD_URL');
		$threadHtml = $this->crawler->openPage($threadPage . $threadId, $jar);
		$postLinks = $this->domParser->parsePostLinksFromThread($threadHtml);

		foreach ($postLinks as $link) {
			$postHtml = $this->crawler->openPage($link, $jar);
			$postData = $this->domParser->parsePost($postHtml);
			echo $postData->title . '<br>';
			$this->fileManager->savePostInFile($postData);
		}
	}
}