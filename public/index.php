<?php
define("BASE_DIR", __DIR__ . '/../');

require_once BASE_DIR . 'vendor/autoload.php';

$dotenv = new \Symfony\Component\Dotenv\Dotenv;
$dotenv->load(BASE_DIR . '.env');

// Create Router instance
$router = new \Bramus\Router\Router();
$router->setNamespace('\App\Controller');

require_once BASE_DIR . 'src/route.php';

$router->run();
